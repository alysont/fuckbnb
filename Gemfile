# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'aws-sdk', '~> 2.8'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'coffee-rails', '~> 4.2'
gem 'devise', '~> 4.2'
gem 'geocoder', '~> 1.4'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'jquery-ui-rails', '~> 5.0'
gem 'puma', '~> 3.0'
gem 'rails', '~> 5.0.3'
gem 'omniauth', '~> 1.6'
gem 'omniauth-facebook', '~> 4.0'
gem 'paperclip', '~> 5.1.0'
gem 'sass-rails', '~> 5.0'
gem 'sqlite3'
gem 'toastr-rails', '~> 1.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'ransack', '~> 1.7'
gem 'twilio-ruby', '~> 4.11.1'
gem 'fullcalendar-rails', '~> 3.4.0'
gem 'momentjs-rails', '~> 2.17.1'
gem 'stripe', '~> 3.0.0'
gem 'rails-assets-card', source: 'https://rails-assets.org'
gem 'omniauth-stripe-connect', '~> 2.10.0'
gem 'chartkick', '~> 2.2.4'

group :development, :test do
  gem 'byebug', platform: :mri
  gem 'pry-rails'
  gem 'dotenv-rails', require: 'dotenv/rails-now'
  gem 'license_finder'
end

group :development do
  gem "better_errors"
  gem "binding_of_caller"
  gem 'listen', '~> 3.0.5'
  gem 'meta_request'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
  gem "meta_request"
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.0.5'
  gem 'rubocop'
  gem 'brakeman'
  gem 'bundler-audit'
  gem 'rack-mini-profiler'
  gem 'bullet'
  gem 'rufo'
  gem 'annotate'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
