class SettingsController < ApplicationController
  before_action :set_setting, only: [:edit, :update]

  def edit
  end

  def update
    if @setting.update(setting_params)
      flash[:notice] = "Saved..."
    else
      flash[:alert] = "Cannot save..."
    end
    render 'edit'
  end

  private

  def setting_params
    params.require(:setting).permit(:enable_sms, :enable_email)
  end

  def set_setting
    @setting = User.find(current_user.id).setting
  end
end
