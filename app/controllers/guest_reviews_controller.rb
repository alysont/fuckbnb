class GuestReviewsController < ApplicationController

  def create

    @reservation = Reservation.where(
      id: guest_reviews_params[:reservation_id],
      room_id: guest_reviews_params[:room_id],
    ).first

    if !@reservation.nil? && @reservation.room.user.id == guest_reviews_params[:host_id].to_i

      @has_revierd = GuestReview.where(
        reservation_id: @reservation.id,
        host_id: guest_reviews_params[:host_id],
      ).first

      if @has_revierd.nil?
        flash[:success] = "Review created.."
      else
        flash[:success] = "You already reviewed this Reservation"
      end

    else
      flash[:alert] = "Not found this reservation"
    end

    @guest_review = current_user.guest_reviews.create(guest_reviews_params)
    redirect_back(fallback_location: request.referer)
  end

  def destroy
    @guest_review = Review.find(params[:id])
    @guest_review.destroy

    redirect_back(fallback_location: request.referer, notice: "Removed..!")
  end


  private

  def guest_reviews_params
    params.require(:guest_review).permit(:comment, :star, :room_id, :reservation_id, :host_id, :guest_id)
  end
end
