require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

MAILHOST = ENV['MAILHOST']
MAILPORT = ENV['MAILPORT']
MAILUSER = ENV['MAILUSER']
MAILPASS = ENV['MAILPASS']
MAILDOMAIN = ENV['MAILDOMAIN']

FACEBOOK_API_KEY = ENV['FACEBOOK_API_KEY']
FACEBOOK_API_SECRET = ENV['FACEBOOK_API_SECRET']

GOOGLEMAP_API_KEY = ENV['GOOGLEMAP_API_KEY']

S3_HOST_NAME = ENV['S3_HOST_NAME']
S3_BUCKET_NAME = ENV['S3_BUCKET_NAME']
S3_ACCESS_KEY_ID = ENV['S3_ACCESS_KEY_ID']
S3_SECRET_ACCESS_KEY = ENV['S3_SECRET_ACCESS_KEY']
S3_REGION = ENV['S3_REGION']

TWILIO_SID = ENV['TWILIO_SID']
TWILIO_AUTH_TOKEN = ENV['TWILIO_AUTH_TOKEN']
TWILIO_PHONE_NUMBER = ENV['TWILIO_PHONE_NUMBER']

STRIPE_PUBLISHABLE_KEY = ENV['STRIPE_PUBLISHABLE_KEY']
STRIPE_SECRET_KEY = ENV['STRIPE_SECRET_KEY']
STRIPE_CONNECT_CLIENT_ID = ENV['STRIPE_CONNECT_CLIENT_ID']

module Fucknb
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
